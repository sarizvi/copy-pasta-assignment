## Mini DeFi Project
This is a simple application that allows users to retrieve the Ethereum token address of a given Crypto Token name. It utilizes Coingecko's API to fetch the token address and stores the data in a database using SQLalchemy for caching purposes. The application provides two functionalities: making an external API call and performing a cached query from the database.

## Requirements
- Python 3.8 or higher
- Flask
- SQLAlchemy
- Unittests
- Coingecko API account and API key

## Installation
- Clone the repository: ```git clone <repository-url>```
- Navigate to the project directory: ```cd mini-defi-project```
- Install the dependencies: ```pip install -r requirements.txt``` 
- Set up the environment variables:
Create a .env file in the project directory and add the following environment variables:

# Coingecko API Base URL
COINGECKO_API_BASE_URL=<Coingecko API base URL>

# Database connection string (e.g., MySQL)
DB_CONNECTION=<database-connection-string>
Make sure to replace <Coingecko API base URL> with the actual Coingecko API base URL and <database-connection-string> with the connection string for your chosen database.

## Usage
- Run the Flask application: ```python app.py```
- Open your web browser and access the following URL: http://localhost:5000/token/<token_name>

If the token address is already cached in the database, it will be retrieved from the database and displayed on the page.

If the token address is not cached, the application will make an API call to Coingecko to fetch the token address, store it in the database, and display it on the page.

## Deployed Version
Contact author for the working prototype.

## Contributing
Contributions are welcome! If you have any suggestions, bug reports, or feature requests, please open an issue or submit a pull request.
