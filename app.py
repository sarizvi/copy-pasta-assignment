from flask import Flask, jsonify
from flask.views import MethodView
from dotenv import load_dotenv
import os
import requests
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from db import CoinAddress

# load env
load_dotenv()

# create connection with db
engine = create_engine(os.getenv("DB_CONNECTION"), echo=True)
Session = sessionmaker(bind=engine)
session = Session()

app = Flask(__name__)

coingecko_base_url = os.getenv("COINGECKO_API_BASE_URL")

# get address from coingecko api and store it in db for future
def get_ethereum_address(token_name: str, token_id: str):
    coingecko_coin_endpoint = f"{coingecko_base_url}coins/{token_id}"
    address_response = requests.get(coingecko_coin_endpoint)
    if address_response.status_code == 200:
        result = {
            "source": "API",
            "address": address_response.json()["platforms"].get("ethereum", ""),
        }
        new_address = CoinAddress(name=token_name, address=result["address"])
        session.add(new_address)
        session.commit()
        return result
    else:
        return None

class TokenAddressView(MethodView):
    def get(self, token_name: str):
        token_name = token_name.strip()

        # check if record is already there in db and return from db if present
        record = session.query(CoinAddress).filter_by(name=token_name).first()
        if record:
            return jsonify({"source": "database", "address": record.address})

        # get token id from name from coingecko api
        coingecko_api_list_endpoint = f"{coingecko_base_url}coins/list"
        response = requests.get(coingecko_api_list_endpoint)
        if response.status_code == 200:
            token_id = None
            for result in response.json():
                if result["name"] == token_name:
                    token_id = result["id"]
                    break
            # get address using token id
            result = get_ethereum_address(token_name, token_id)
            if not result:
                # if result is not present from api
                return jsonify({"error": "coin not found!"})
            return jsonify(result)
        # if API throws error
        return jsonify({"error": "API error"})


app.add_url_rule(
    "/token/<token_name>", view_func=TokenAddressView.as_view("token_address_view")
)

if __name__ == "__main__":
    app.run(debug=True)
