import unittest
from unittest.mock import patch
from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from db import CoinAddress
import os
from app import get_ethereum_address
from dotenv import load_dotenv

load_dotenv()


class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data


class TokenAddressViewTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        app = Flask(__name__)
        cls.client = app.test_client()

        cls.engine = create_engine(os.getenv("DB_CONNECTION"))
        cls.Session = sessionmaker(bind=cls.engine)


    def setUp(self):
        self.session = self.Session()

    def tearDown(self):
        self.session.close()
        self.engine.dispose()

    @patch("requests.get")
    def test_get_ethereum_address_success(self, mock_get):
        token_id = "example_token_id"
        response_data = {
            "platforms": {
                "ethereum": "0x123456",
            },
        }
        mock_get.return_value = MockResponse(response_data, 200)

        result = get_ethereum_address("Example", token_id)

        self.assertEqual(result["source"], "API")
        self.assertEqual(result["address"], response_data["platforms"]["ethereum"])

        address = self.session.query(CoinAddress).filter_by(name="Example").first()
        self.assertIsNotNone(address)
        self.assertEqual(address.address, result["address"])

    def test_token_address_view_from_database(self):
        address = CoinAddress(name="Example", address="0x123456")
        self.session.add(address)
        self.session.commit()

        response = self.client.get("/token/Example")
        address_from_db = self.session.query(CoinAddress).filter_by(name="Example").first()
        self.assertEqual(
            response.get_json(), {"source": "database", "address": address_from_db.address}
        )

