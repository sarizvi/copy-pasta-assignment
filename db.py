from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from dotenv import load_dotenv
import os

load_dotenv()

engine = create_engine(os.getenv("DB_CONNECTION"), echo=True)

Base = declarative_base()


class CoinAddress(Base):
    __tablename__ = "coin_address"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))
    address = Column(String(60))


Base.metadata.create_all(engine)
